from selenium import webdriver
from selenium.webdriver import ChromeOptions
from selenium.webdriver import FirefoxOptions
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
import pytest

@pytest.fixture(scope='function', autouse=True)
# def driver(request):
#     """Create driver object"""
#     options = ChromeOptions()
#     options.add_argument("no-sandbox")
#     options.accept_untrusted_certs = True
#     options.assume_untrusted_cert_issuer = True
#     options.add_argument("--disable-infobars")
#     options.add_argument("--headless")
#     driver_ = webdriver.Chrome(ChromeDriverManager().install(), options=options)
#     yield driver_
#
#     driver.quit()
#

def driver(request):
    options = FirefoxOptions()
    options.add_argument("no-sandbox")
    options.accept_untrusted_certs = True
    options.assume_untrusted_cert_issuer = True
    options.add_argument("--disable-infobars")
    options.add_argument("--headless")
    driver_ = webdriver.Firefox(executable_path=GeckoDriverManager().install(), options=options)
    yield  driver_

    driver_.quit()


