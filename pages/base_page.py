from allure import step

class BasePage:

    base_url = 'http://t.deco-creative.com.ua'

    def __init__(self, driver):
        self.driver = driver

    @step("Open homepage")
    def open(self):
        self.driver.get(self.base_url)
