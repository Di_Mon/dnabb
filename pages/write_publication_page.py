from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from pages.base_page import BasePage
from pages.user_account import SignIn

class Publications(BasePage):

    def write_publications(self):
        write_public_button = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, "//*[@class='add-publication']"))
        )

        write_public_button.click()

        preview_button = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, "//*[@value='Попередній перегляд']"))
        )