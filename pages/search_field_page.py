from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from pages.base_page import BasePage
import random
from selenium.common.exceptions import TimeoutException
import string
import time

class SearchField(BasePage):

    search = (By.XPATH, "//*[@name='search_block_form']")
    linsa = (By.XPATH, "//*[@name='op']")

    relevant_words = ["архітектор", "заболотний", "альошин", "мистецтво", "бібліотека", "бекетов", "лисенко"]

    # Make a search from main page
    def make_search(self, expression_to_search):
        search_field = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located(self.search)
        )
        search_field.click()
        search_field.send_keys(expression_to_search)
        search_field.submit()
        search_field_result = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, "//*[@name='keys']"))
        )
        return search_field_result.get_attribute('value')

    def make_search_from_search_page(self, expression_to_search):
        search_field_result = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, "//*[@id='edit-keys']"))
        )
        search_field_result.clear()
        search_field_result.send_keys(expression_to_search)
        search_field_result.submit()
        # return expression_to_search in self.driver.current_url
        return search_field_result.get_attribute('value')

    # search field is displayed on the page
    def search_field_is_shown(self):
        try:
            search_field = WebDriverWait(self.driver, 35).until(
                EC.presence_of_element_located(
                    (By.XPATH, "//*[@name='search_block_form']"))
            )
            return search_field.is_displayed()
        except TimeoutException:
            return False

    # checks if the value of new search field is equal to search
    def search_results_is_shown(self, search_expression):
        return self.make_search(search_expression) == search_expression

    # search with max length of the search field
    def search_max_lenth(self):
        expression_to_search = ''.join([random.choice(string.ascii_letters + string.digits  ) for n in range(128)])
        return self.search_results_is_shown(expression_to_search)

    # empty search
    def search_no_expression(self):
        expression_to_search = ''
        return self.search_results_is_shown(expression_to_search)

    # takes a random relevant search from list and compare results
    def relevant_search(self):
        expression_to_search = self.relevant_words[random.randint(0, len(self.relevant_words)-1)]
        return self.search_results_is_shown(expression_to_search)

    # There must not be any discrepancy in result when searching with same keyword multiple times.

    # How feasible it is for user to navigate back to same search result page after clicking on any result link?

    # Facility to search separate multiply words in one request and result for each of them, not only together.