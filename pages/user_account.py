from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from pages.base_page import BasePage

class SignIn(BasePage):

    def enter_login(self, login_name):
        username = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="edit-name"]'))
        )
        username.click()
        return username.send_keys(login_name)

    def enter_password(self, password):
        enter_password = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="edit-pass"]'))
        )
        enter_password.click()
        return enter_password.send_keys(password)

    def confirm_button_click(self):
        submit_name = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="edit-submit"]'))
        )
        return submit_name.click()

    def sign_in(self):
        sign_in_button = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[@class="login "]'))
        )
        sign_in_button.click()
        login_name = "admin"
        self.enter_login(login_name)
        password = '123'
        self.enter_password(password)
        self.confirm_button_click()

    def sign_out(self):
        sign_out_button = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//a[@class="logout"]'))
        )
        return sign_out_button.click()

    def registration_new_user(self):
        sign_in_button = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located(
                (By.XPATH, '//a[contains(@class, "login")]'))
        )
        sign_in_button.click()
        registrarion_page = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//a[@class = "active"]'))
        )
        registrarion_page.click()
        name_field = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="edit-name"]'))
        )
        name_field.click()
        name_field.send_keys("Dino Djoff")
        mail_field = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="edit-mail"]'))
        )
        mail_field.click()
        mail_field.send_keys('igrok_6@ukr.net')
        password = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="edit-pass-pass1"]'))
        )
        password.click()
        password.send_keys('inter')
        self.confirm_button_click()

        profile_button = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '//*[@class = "profile "]'))
        )

        return profile_button

