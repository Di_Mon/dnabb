from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from pages.base_page import BasePage
from allure import step


class ResourcePage(BasePage):
    @step("Check if resources shown")
    def is_shown(self):
        try:
            resources = WebDriverWait(self.driver, 35).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[1]/header/div/div/div[2]/div[1]/ul/li[2]/a"))
            )
            return resources.is_displayed()
        except:
            raise Exception("Resources button not found!")

    def is_clickable(self):
        try:
            resources = WebDriverWait(self.driver, 35).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[1]/header/div/div/div[2]/div[1]/ul/li[2]/a"))
            )
            resources.click()
        except:
            raise Exception("Element is not clickable.")

    # Checking if 10 pictures available in new arrivals post
    def pictures_is_available(self):
        resources = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located(
                (By.XPATH, "//*[@class='header-content__main-menu']//*[text()='Ресурси бібліотеки']"))
        )
        resources.click()
        new_arrivals_april = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located(
                (By.XPATH, '//*[text()="Нові надходження: квітень 2019 року"]'))
        )
        new_arrivals_april.click()
        pictures = WebDriverWait(self.driver, 35).until(
            EC.presence_of_all_elements_located((By.XPATH, '//*[@id="node-409"]/div/div[1]/div/div//img'))
        )
        return len(pictures)

    # Check if the correct article is opened. Title of preview == title of article
    def correct_article_is_opened(self):
        resources = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located(
                (By.XPATH, "/html/body/div/div/div[1]/header/div/div/div[2]/div[1]/ul/li[2]/a"))
        )
        resources.click()
        new_arrivals_december = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located(
                (By.XPATH, '/html/body/div/div/div[2]/div[3]/div[1]/div/div[2]/div[1]/div/div[1]/div/div[1]/a'))
        )
        title_preview = new_arrivals_december.text
        new_arrivals_december.click()
        title_article = WebDriverWait(self.driver, 35).until(
            EC.presence_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div/div/h1'))
        ).text
        return title_article == title_preview
