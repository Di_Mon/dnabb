import pytest
import allure
from pages.resources_page import ResourcePage
from tests.base_test import BaseTest
from pages.user_account import SignIn

@pytest.mark.usefixtures('driver')
class TestSignIn():

    @allure.title("This test has a custom title")
    def test_sign_in(self, driver):
        resource = ResourcePage(driver)
        resource.open()
        users = SignIn(driver)
        users.sign_in()
        assert "admin" in str(self.driver.current_url)

    def test_sign_out(self, driver):
        resource = ResourcePage(driver)
        resource.open()
        users = SignIn(driver)
        users.sign_in()
        users.sign_out()
        assert "admin" not in str(self.driver.current_url)

    def test_register_new_user(self, driver):
        resource = ResourcePage(driver)
        resource.open()
        users = SignIn(driver)
        assert users.registration_new_user()
