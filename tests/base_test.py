import unittest

from selenium import webdriver

from driver.driver_manager import DriverManager


class BaseTest(unittest.TestCase):

    def setUp(self):
        self.manager = DriverManager()
        self.driver = webdriver.Chrome(self.manager.get_path_to_geckodriver())
        self.driver.implicitly_wait(30)

    def tearDown(self):
        self.driver.quit()
