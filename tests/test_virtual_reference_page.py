import pytest
import allure
from pages.virtual_reference_page import VirtualReferencePage

@pytest.mark.usefixtures('driver')
class VirtualReferenceTest():
    @allure.title("This test has a custom title")
    def test_virtual_reference_button_is_shown(self, driver):
        users = VirtualReferencePage(driver)
        users.open()
        virtual_ref = VirtualReferencePage(driver)
        assert virtual_ref.virtual_reference_button_is_shown()

    def test_virtual_regerence_button_is_clickable(self, driver):
        users = VirtualReferencePage(driver)
        users.open()
        virtual_ref = VirtualReferencePage(driver)
        virtual_ref.virtual_reference_button_click()
        assert 'faq' in str(self.driver.current_url)

    def test_ask_question(self, driver):
        users = VirtualReferencePage(driver)
        users.open()
        virtual_ref = VirtualReferencePage(driver)
        virtual_ref.virtual_reference_button_click()
        assert virtual_ref.ask_question()

    def test_choose_category_dropdown(self, driver):
        users = VirtualReferencePage(driver)
        users.open()
        virtual_ref = VirtualReferencePage(driver)
        virtual_ref.virtual_reference_button_click()
        virtual_ref.faq_search()
        assert 'results' in str(self.driver.current_url())
