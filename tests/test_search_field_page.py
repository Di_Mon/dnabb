import pytest
import allure
from pages.resources_page import ResourcePage
from pages.search_field_page import SearchField

@pytest.mark.usefixtures('driver')
class TestSearchField():

    @allure.title("This test has a custom title")
    # Testing if Search field available on page
    def test_is_shown(self, driver):
        resources = ResourcePage(driver)
        search = SearchField(driver)
        resources.open()
        assert search.search_field_is_shown()

    def test_search_max_length(self, driver):
        resources = ResourcePage(driver)
        search = SearchField(driver)
        resources.open()
        assert search.search_max_lenth()

    def test_search_no_expression(self, driver):
        resources = ResourcePage(driver)
        search = SearchField(driver)
        resources.open()
        search.search_no_expression()
        assert search.search_no_expression()

    def test_relevant_search(self, driver):
        resources = ResourcePage(driver)
        search = SearchField(driver)
        resources.open()
        # search.search_no_expression()
        assert search.relevant_search()
