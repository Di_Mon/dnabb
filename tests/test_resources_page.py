from pages.resources_page import ResourcePage
import pytest
import allure


@pytest.mark.usefixtures('driver')
class TestResources:
    @allure.title("This test has a custom title")
    # Testing if Resources button available on desktop
    def test_is_shown(self, driver):
        resources = ResourcePage(driver)
        resources.open()
        assert resources.is_shown()

    # Tests is Resources button is clickable
    def test_is_clickable(self, driver):
        resources = ResourcePage(driver)
        resources.open()
        resources.is_clickable()
        assert "resources" in str(driver.current_url)

    # Checks if 10 pictures available in new arrivals post
    def test_new_arrivals_pictures(self, driver):
        resources = ResourcePage(driver)
        resources.open()
        assert resources.pictures_is_available() == 10

    # Checks if title of preview and a title of opened page in new arrivals are equals
    def test_correct_article_is_opened(self, driver):
        resources = ResourcePage(driver)
        resources.open()
        assert resources.correct_article_is_opened()
